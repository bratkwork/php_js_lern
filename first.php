<?php
    echo __FILE__ . "\n";
    echo 'We stat!' . "\n";

    var_dump(true ? 0 : true ? 1 : 2); // (true ? 0 : true) ? 1 : 2 = 2
    var_dump(true ? 0 : false ? 1 : 2);
    
    var_dump(true ? 0 : true ? 0 : true ? 3 : 2);
    var_dump((true ? 0 : true ? 0 : true) ? 3 : 2);
    var_dump(((true ? 0 : true) ? 0 : true) ? 3 : 2);
?>

<script>
    console.log(123);
</script>

//    var_dump(true ? false ? 30 : 10 : 20);
//    var_dump(true ? (false ? 30 : 10) : 20);
//
//    var_dump(true ? true ? 30 : 10 : 20);
//    var_dump(true ? true ? 30 : 10 : 20);

//    $a = true ? 0 : true ? 1 : 2; // (true ? 0 : true) ? 1 : 2 = 2

