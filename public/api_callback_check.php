<?php
    $secret = getEnv('CALLBACK_SECRET_KEY');
    $fileName = 'test_file_'. (new \DateTime())->format('Y-m-d_H-i-s') . '.txt';
    ob_start();

    if (!isset($_SERVER['HTTP_X_DATA_SIGNATURE'])) {
        echo 'Ошибка: В заголовках подписи не найдено!';
        file_put_contents('/tmp/' . $fileName, ob_get_contents());
        ob_end_clean();
        exit;
    }

    $data = file_get_contents('php://input');

    $signature = hash_hmac('sha256', $data, $secret);
    if ($signature != $_SERVER['HTTP_X_DATA_SIGNATURE']) {
        echo 'Ошибка: Подпись не соответствует!';
        file_put_contents('/tmp/' . $fileName, ob_get_contents());
        ob_end_clean();
        exit;
    }

    echo $data;

    file_put_contents('/tmp/' . $fileName, ob_get_contents());
    ob_end_clean();
